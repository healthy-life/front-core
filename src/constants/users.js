export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST'
export const FETCH_USERS_DONE = 'FETCH_USERS_DONE'
export const ADD_FETCHED_USERS = 'ADD_FETCHED_USERS'
export const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING'
