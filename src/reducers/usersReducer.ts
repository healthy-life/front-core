import { ADD_FETCHED_USERS, TOGGLE_IS_FETCHING } from '../constants/users'
const initialState = {
  users: [],
  isFetching: false
}
export const usersReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case TOGGLE_IS_FETCHING:
      return {
        ...state,
        isFetching: action.payload
      }
    case ADD_FETCHED_USERS:
      return {
        ...state,
        users: [...state.users, ...action.payload]
      }
    default: return state
  }
}