import React from 'react';
import Users from './components/Users'

const App: React.FC = () => {
  return (
    <div className="app">
      <Users />
    </div>
  )
};

export default App
