import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchUsersRequest } from '../actions/users'
import Loader from 'react-loader-spinner'
import './syles.css'

const Users: React.FC = () => {
  const dispatch = useDispatch()
  const users = useSelector((state: any) => state.users)
  const { isFetching } = users
  return (
    <div className="users-container">
      {isFetching &&
      <div className="loader-container">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        />
      </div>
      }
      {!isFetching &&
      <button onClick={() => dispatch(fetchUsersRequest())}>Reveal users</button>
      }
    </div>
  )
}

export default Users