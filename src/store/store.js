import { createStore, applyMiddleware } from 'redux';
import {composeWithDevTools } from 'redux-devtools-extension'
import { rootReducer } from './rootReducer'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './rootSaga'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk, sagaMiddleware)
))
sagaMiddleware.run(rootSaga)

export default store