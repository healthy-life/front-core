import { fork, takeEvery, put, take } from 'redux-saga/effects'
import { FETCH_USERS_REQUEST, FETCH_USERS_DONE } from '../constants/users'
import { fetchUsers, addFetchedUsers, toggleIsFetching } from '../actions/users'


function* getUsersSaga() {
  yield put(toggleIsFetching(true))
  yield put(fetchUsers())
  const { payload } = yield take(FETCH_USERS_DONE)
  yield put(addFetchedUsers(payload))
  yield new Promise(resolve =>
    setTimeout(resolve, 2000)
  )

  yield put(toggleIsFetching(false))
}

function* watchGetUsersSaga() {
  yield takeEvery(FETCH_USERS_REQUEST, getUsersSaga)
}

export default function*() {
  yield fork(watchGetUsersSaga)
}