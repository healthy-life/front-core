import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_DONE,
  ADD_FETCHED_USERS,
  TOGGLE_IS_FETCHING
} from '../constants/users'

export function fetchUsersRequest() {
  return {
    type: FETCH_USERS_REQUEST
  }
}
export function fetchUsers() {
  return async (dispatch: any) => {
    const response = await fetch('http://localhost:8080/api/v1/users')
    const json = await response.json()
    dispatch({ type: FETCH_USERS_DONE, payload: json.content })
  }
}

export function addFetchedUsers(payload: any) {
  return {
    type: ADD_FETCHED_USERS,
    payload
  }
}

export function toggleIsFetching(payload: boolean) {
  return {
    type: TOGGLE_IS_FETCHING,
    payload
  }
}